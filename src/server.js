import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import mongoose from 'mongoose';
import UserRoutes from './routes/userRoutes'

import passport from 'passport';
import flash from 'connect-flash';
import session from 'express-session';
import expressJwt from 'express-jwt';

var env = process.env.NODE_ENV || 'development'
var config = require('../config/' + env);

class Server {
    constructor(){
        this.app = express();
    }

    configureDatabase(){
        mongoose.connect(config.database.mongo.connectionString);
    }

    configureApp() {
        this.app.set('port', (process.env.PORT || config.port));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
    }

    configureCORS(){
        // Additional middleware which will set headers that we need on each request.
        this.app.use((req, res, next) => {
            // Set permissive CORS header - this allows this server to be used only as
            // an API server in conjunction with something like webpack-dev-server.
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, DELETE, GET');
            res.setHeader('Access-Control-Allow-Headers', 'content-type, x-xsrf-token, Access-Control-Allow-Headers, Authorization, X-Requested-With');
            res.setHeader('Cache-Control', 'no-cache');
            next();
        });
    }

    configurePassport(){
        require('./routes/dataHandlers/passport')(passport);
        // required for passport
        this.app.use(session({ secret: 'sessionsecretstring',  resave: true, saveUninitialized: true })); // session secret
        this.app.use(passport.initialize());
        this.app.use(passport.session()); // persistent login sessions
        this.app.use(flash()); // use connect-flash for flash messages stored in session
    }

    configureRoutes(){
        
        this.app.use('/api', expressJwt({secret: '09qrjjwef923jnrgxcvxc5ndjwk'}));

        // Point static path to dist if not development
        if(env != 'development'){
            this.app.use(express.static(path.join(__dirname, '../../client/mongo')));//path do the client dist folder
        }

        //Initialize application routes
        new UserRoutes(this.app, passport);

        // Catch all other routes and return the index file
        if(env != 'development'){
            this.app.get('*', (req, res) => {
                res.sendFile(path.join(__dirname, '../../client/mongo/index.html'));//path do the client entry point (eg. index.html)
            });
        }
    }

    listen(port){
        this.app.listen(port, () => {
            console.log(`Server started: http://localhost:${port}/`);
        });
    }

    run(){
        console.log('Configuring database...');
        this.configureDatabase();

        console.log('Configuring application...');
        this.configureApp();
        this.configureCORS();
        this.listen(this.app.get('port'));

        console.log('Configure passport');
        this.configurePassport();

        console.log('Initializing routes...');
        this.configureRoutes();
    }
}

export default Server;
