import User from '../../models/user';
import Server from '../server';

import chai from 'chai';
import sinon from 'sinon';

import UserDataHandler from '../routes/dataHandlers/userDataHandler';

let userDataHandler = new MembeUserDataHandlerrDataHandler();
let expect = chai.expect;

describe("Routes", function() {
    describe("GET Users", function() {
        var find;
        var req,res,spySend,spyJson; 

        beforeEach(function() {
            req = res = {};
            spyJson = res.send = sinon.spy();

            var mockFind = {
                where: function () {
                    return this;
                },
                equals: function () {
                    return this;
                },
                exec: function(err, users){
                    if(err)
                        res.send(err);

                    res.json(users);
                }
            };

            find = sinon.stub(User, "find").returns(mockFind);
        });

        afterEach(function() {
            find.restore();
        });

        it("should call db find once", sinon.test(function() {
            userDataHandler.getUsers(req, res);
            expect(User.find.calledOnce).to.equal(true);
        }));

        it("should return json response", sinon.test(function() {
            expect(spyJson.calledOnce).to.equal(true);
        }));     
    });
});

function handleError(done, fn) {
    try { // boilerplate to be able to get the assert failures
        fn();
        done();
    } catch (error) {
        done(error);
    }
}

//Examples:
// expect(err).to.be.null;
// expect(res).to.have.status(200);