import UserDataHandler from './dataHandlers/userDataHandler';
import jwt from 'jsonwebtoken';

var env = process.env.NODE_ENV || 'development'
var app = require('../../config/' + env);

class UserRoutes {
    constructor(express, passport){
        this.app = express;
        this.passport = passport;
        
        this.userDataHandler = new UserDataHandler(this.passport);
        this.app.post('/authenticate', this.userDataHandler.login, generateToken, respond);
        this.app.post('/addUser', this.userDataHandler.addUser, respond); 
        this.app.post('/api/editUser', this.userDataHandler.editUser, respond);
    }
}

//passport respond middleware
function respond(req, res) { 
    res.status(200).json({
        uid: res.req.user._id,
        email: res.req.user.email, 
        password: res.req.user.password,
        displayName: res.req.user.displayName,
        photoURL: res.req.user.photoURL, 
        token: req.token
    });
}

//token generation middleware
function generateToken(req, res, next) {  
  if(!req.token){
	console.log('generating token');
	req.token = jwt.sign(req.body.email, '09qrjjwef923jnrgxcvxc5ndjwk');
  }
  next();
}

export default UserRoutes;