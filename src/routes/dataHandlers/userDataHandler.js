import User from '../../../models/user';

var passport = null;

class UserDataHandler {
    constructor(psprt){
        passport = psprt;
    }

    login(req, res, next){
		passport.authenticate('local-login', {
            failureflash : true
        })(req, res, next);
	}

    addUser(req, res, next){
        passport.authenticate('local-register', {
            failureflash : true
        })(req, res, next);
	}

    editUser(req, res, next){
        passport.authenticate('local-update', {
            failureflash : true
        })(req, res, next);
    }
}

export default UserDataHandler;