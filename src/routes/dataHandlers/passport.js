var LocalStrategy   = require('passport-local').Strategy;
var User = require('../../../models/user');

module.exports = function(passport) {
    console.log('passport configure');
    
    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
      passport.use('local-register', new LocalStrategy({ 
            usernameField : 'email', 
            passwordField : 'password', 
            passReqToCallback : true },
        function(req, email, password, done) {
            process.nextTick(function() {
                User.findOne({ 'email' :  email }, function(err, user) {
                    if (err){
                        console.log(err);
                        return done(err);
                    }

                    if (user) {
                        return done(null, false);
                    } 
                    else {
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.email = email;
                        newUser.password = newUser.generateHash(password);
                        newUser.firstName = req.body.firstName;
                        newUser.lastName = req.body.lastName;
                        newUser.displayName = req.body.firstName + " " + req.body.lastName;
                        newUser.photoURL = req.body.photoURL;
                        newUser.uid = newUser._id;

                        // save the user
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });    
            });
        })
    );
	
	// =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
	passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {
        User.findOne({ 'email' :  email }, function(err, user) {
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user){
				console.log('No user found with that email!')
                return done(null, false);
			}

            // if the user is found but the password is wrong
            if (!user.validPassword(password)){
				console.log('Wrong password!')
                return done(null, false);
			}

			console.log('User logged in successfully.');

            // all is well, return successful user
            return done(null, user);
        });

    }));

    // =========================================================================
    // LOCAL UPDATE =============================================================
    // =========================================================================
	passport.use('local-update', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) { 
        var updates = {
            email: email,
            displayName: req.body.displayName,
            photoURL: req.body.photoURL
        };

        User.findOneAndUpdate({ '_id' :  req.body.uid }, updates, function(err, user) {
            if (err)
                return done(err);
            
            user.email = updates.email ? updates.email : user.email;
            user.displayName = updates.displayName ? updates.displayName : user.displayName;
            user.photoURL = updates.photoURL ? updates.photoURL : user.photoURL;

            return done(null, user);
        });
    }));
};